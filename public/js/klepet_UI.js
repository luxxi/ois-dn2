function divElementEnostavniTekst(sporocilo) {
  //najdi znake in jih zamenjaj za smeskote
  var DBurl = 'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/';
  var smeskoti = [ [';)', 'wink.png'], 
                   [':)', 'smiley.png'],
                   ['(y)', 'like.png'], 
                   [':*', 'kiss.png'],
                   [':(', 'sad.png'] ];
  var smesko = false;

  for (var i = 0; i < smeskoti.length; i++) {
    var count = 0;
    var pos = sporocilo.indexOf(smeskoti[i][0]);
    while( pos != -1 ){
      smesko = true;
      sporocilo = sporocilo.replace(smeskoti[i][0], '<img src="' + DBurl + smeskoti[i][1] + '" />'); 
      count++;
      pos = sporocilo.indexOf(smeskoti[i][0], pos+1);
    } 
  };
  if (smesko == true) {
    return $('<div></div>').html(sporocilo);
  };
  return $('<div style="font-weight: bold"></div>').text(sporocilo);  
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    sporocilo = klepetApp.filtrirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    $('#vzdevek').html('').append(rezultat.vzdevek);
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('uporabniki', function (uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i = 0; i < uporabniki.length; i++) {
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
    };
  });

  socket.on('odstraniUporabnikaIzSeznama', function(uporabnik) {
    $('#seznam-uporabnikov').find('div').each(function() {
      if($(this).text() == uporabnik)
        $(this).remove();
    });
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});