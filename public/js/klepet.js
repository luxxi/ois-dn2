var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.filtrirajVulgarneBesede = function(sporocilo){
  $.ajax( {
    url: "/swearWords.txt", 
    success: function( data ) {
      swearWords = data.split('\n');
    },
    async: false
  });
  besede = sporocilo.split(' ');
  swearWords = swearWords.map(function(e){return e.trim()});

  for (var x = 0; x < besede.length; x++) {
    for (var y = 0; y < swearWords.length; y++) {
      if(besede[x] == swearWords[y]){
        var zvezdice = '';
        for (var i = 0; i < besede[x].length; i++) {
          zvezdice += '*';
        };
        besede[x] = zvezdice;
      }
    };
  };
  sporocilo = besede.join(' ');
  return sporocilo;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.spremeniKanal = function(data) {
  var sporocilo = {};

  if((data.match(/"/g) || []).length == 4){
    data = data.split('" "');
    for (var i = 0; i < data.length; i++) {
      data[i] = data[i].replace(/"/g, '');
    };
    sporocilo = {
      kanal: data[0],
      geslo: data[1]
    };
  }else{
    data = data.replace(/"/g, '');
    sporocilo = {
      kanal: data,
      geslo: '' 
    };
  };

  this.socket.emit('pridruzitevZahteva', sporocilo);
};


Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var data = besede.join(' ');
      this.spremeniKanal(data);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      besede = besede.join(' ');
      try {
        var data = besede.split('" "');
        var uporabnik = data[0].replace(/"/g, '');
        var besedilo = data[1].replace(/"/g, '');
        this.socket.emit('zasebnoZahteva', uporabnik, besedilo);
        break;
      }catch(err) {};
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};