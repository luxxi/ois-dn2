var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var vsiKanali = {};

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZasebno(socket, uporabljeniVzdevki);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
  prikaziVseUporabnike(socket, kanal);
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
        prikaziVseUporabnike(socket, trenutniKanal[socket.id]);
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajZasebno(socket, uporabljeniVzdevki) {
  socket.on('zasebnoZahteva', function(uporabnik, besedilo) {
    var odgovor = { besedilo: 'Sporočilo "' + uporabnik + '" uporabniku z vzdevkom ' + besedilo + ' ni bilo mogoče posredovati.' };
    
    if(uporabnik !== vzdevkiGledeNaSocket[socket.id]){
      for(var i in vzdevkiGledeNaSocket){
        if(uporabnik == vzdevkiGledeNaSocket[i]) {
            io.sockets.socket(i).emit('sporocilo', {
              besedilo: vzdevkiGledeNaSocket[socket.id] + ' (zasebno): ' + besedilo
            });
            odgovor.besedilo = '(zasebno za ' + uporabnik + '): ' + besedilo;
            break;
        };
      };
    };
    socket.emit('sporocilo', odgovor);
  });
}

function preveriKanal(novKanal){
  for (var i in vsiKanali){
    if (vsiKanali[i].kanal == novKanal)
      return vsiKanali[i];
  };
  return false;
};

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(sporocilo) {
    var kanal = preveriKanal(sporocilo.kanal);

    if (kanal != false){
      var odgovor = { besedilo: '' };
      if (kanal.geslo == '' && sporocilo.geslo != ''){
        odgovor.besedilo = 'Izbrani kanal ' + sporocilo.kanal + ' je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev ' + sporocilo.kanal + ' ali zahtevajte kreiranje kanala z drugim imenom.';
        socket.emit('sporocilo', odgovor);
        return;
      };

      if (kanal.geslo != sporocilo.geslo){
        odgovor.besedilo = 'Pridružitev v kanal ' + sporocilo.kanal + ' ni bilo uspešno, ker je geslo napačno!';
        socket.emit('sporocilo', odgovor);
        return;
      };
    }else{
      vsiKanali[socket.id] = sporocilo;
    };
    socket.leave(trenutniKanal[socket.id]);
    pridruzitevKanalu(socket, sporocilo.kanal);
  });
}

function obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    odstraniUporabnika(socket, uporabljeniVzdevki[vzdevekIndeks]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}

function prikaziVseUporabnike(socket, kanal){
  var uporabniki = [];
  for (var i = 0; i < io.sockets.clients(kanal).length; i++) {
    uporabniki[i] = vzdevkiGledeNaSocket[io.sockets.clients(kanal)[i].id];
  };
  io.sockets.in(kanal).emit('uporabniki', uporabniki);
}

function odstraniUporabnika(socket, uporabnik) {
  socket.broadcast.emit('odstraniUporabnikaIzSeznama', uporabnik);
}